<?php
    $token = 'iahASFHI@!3f1qp39gp2cg__aslkdacdlok22ASD!';

    // Token required for using this script - this helps stop spammers from using this script
    if (isset($_POST['token']) && trim($_POST['token']) == $token) {
        // don't need to do anything if token is correct
    } else {
        die('You must supply the correct token to use this mail script.');
    }

    //IF THE from_email TAG IS NOT IN THE HTML THEN SET YOU NEED THE FOLLOWING
    // EXAMPLE: <input type='hidden' name='from_email' value='something@something.com' />
    if (!$_POST['email']) {
        $from_email = 'fromemail@needed.com';
    } else {
        $from_email = trim($_POST['email']);
    }
    //IF THE subject TAG IS NOT IN THE HTML THEN SET YOU NEED THE FOLLOWING
    // EXAMPLE: <input type='hidden' name='subject' value='A Subject' />
    if (!$_POST['subject']) {
        $subject = 'SUBJECT HERE';
    } else {
        $subject = trim($_POST['subject']);
    }
    //IF THE cc TAG IS NOT IN THE HTML THEN SET YOU NEED THE FOLLOWING
    // EXAMPLE: <input type='hidden' name='cc' value='something@something.com' />
    if (!$_POST['cc']) {
        $cc = '';
    } else {
        $cc = trim($_POST['cc']);
    }
    //IF THE to TAG IS NOT IN THE HTML THEN SET YOU NEED THE FOLLOWING
    // EXAMPLE: <input type='hidden' name='cc' value='something@something.com' />
    if (!$_POST['recipient']) {
        $to = 'khelton57@gmail.com';
    } else {
        $to = trim($_POST['recipient']);
    }
    //IF THE from_name TAG IS NOT IN THE HTML THEN SET YOU NEED THE FOLLOWING
    // EXAMPLE: <input type='hidden' name='from_name' value='Monkey Toe Wilson' />
    if (!$_POST['name']) {
        $from_name = 'NAME NEEDED';
    } else {
        $from_name = trim($_POST['name']);
    }
	//IF THE thanks TAG IS NOT IN THE HTML THEN SET YOU NEED THE FOLLOWING
    // EXAMPLE: <input type='hidden' name='thanks' value='thanks.html' />
    if (!$_POST['thanks']) {
        $thanks = '/ThankYou.html';
    } else {
        $thanks = trim($_POST['thanks']);
    }

	//HTML OR PLAIN TEXT EMAILS : yes = HTML
	$html = 'yes';

    //============================================================================================
    // DO NOT EDIT PAST THS LINE
    //============================================================================================

    $specialPostArray = array('recipient', 'name', 'subject', 'email', 'cc', 'thanks');
    $message = '<html><body><p>You recieved a message from your website.  Here is what was sent:</p>';
	$message .= '<b>Name:</b> '.$from_name.'<br/>';
	$message .= '<b>Email Address:</b> '.$from_email.'<br/>';
    // To send HTML mail, the Content-type header must be set
	if ($html == 'yes') {
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	}
	// Additional headers
	$headers .= 'To: '.$to . "\r\n";
	$headers .= 'From: '.$from_name.' <'.$from_email.'>' . "\r\n";
	if ($cc != '') {
		$headers .= 'Cc: '.$cc . "\r\n";
	}

    foreach ($_POST AS $postKey => $postValue) {
        if (!in_array($postKey, $specialPostArray)) {
            $message .= '<b>'.ucfirst($postKey).'</b>: '.$postValue.'<br/>';
        }
    }

    if ($to != '' && $headers != '' && $subject != '' && $message != '') {
        if (!mail($to, $subject, $message, $headers)) {
            die('Unable to send email using PHP\'s mail method.');
        } else {
			//if thanks exists ... forward ... if not display plain page.
			if ($thanks != '') {
				header("Location: ".$thanks);
			} else {
				echo $message;
				echo '<pre>'.print_r($_POST, true).'</pre>';
			}
		}
    } else {
        /*
		echo 'Headers: '.$headers.'<br>';
        echo 'Message: '.$message.'<br>';
        echo 'Subject: '.$subject.'<br>';
        echo 'To: '.$to.'<br>';
        die('Some of the needed vars are empty!');
		*/
    }
?>